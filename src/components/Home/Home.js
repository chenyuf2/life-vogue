import homeVideo from "../../images/098164fa43fb8952d4d5d28bda22a90a09de77c50dcf0109fab3ceb0cf5d1a47.mp4";
import styles from "./Home.module.scss";
import clsx from "clsx";
import { svgElement, arrowRight, titleSvg } from "../../others/svg";
import { gsap } from "gsap";
import { useEffect, useRef } from "react";
const Home = () => {
  const videoRef = useRef(null);
  const svgRef = useRef(null);
  const footerRef = useRef(null);
  const nextPageRef = useRef(null);
  useEffect(() => {
    videoRef.current.play();
    gsap.delayedCall(0.53, () => {
      videoRef.current.pause();
    });
  });
  const btnClickEvent = () => {
    videoRef.current.play();
    const timelineContainer = gsap.timeline();
    gsap.delayedCall(7.1, () => {
      videoRef.current.pause();
    });
    timelineContainer
      .fromTo(svgRef.current, { opacity: 1 }, { opacity: 0 })
      .fromTo(
        footerRef.current,
        { opacity: 1 },
        { opacity: 0, display: "none" }
      )
      .to(".title-svg-container", { opacity: 1 })
      .to(nextPageRef.current, { opacity: 1, display: "flex", delay: 4.5 });
  };
  return (
    <div className={clsx(styles["home-container"], "position-relative")}>
      <div
        className="title-svg-container position-absolute p-3"
        style={{ opacity: 0 }}
      >
        <div>{titleSvg}</div>
      </div>
      <div className="position-absolute title-container">
        <div className="w-100 h-100" ref={svgRef}>
          {svgElement}
        </div>
      </div>
      <video src={homeVideo} muted ref={videoRef}></video>
      <div
        className={clsx(
          styles["footer-button-container"],
          "von-font text-center"
        )}
        ref={footerRef}
      >
        <h4>
          EXPLORE <br /> THE EVENT
        </h4>
        <div
          className={clsx(
            styles["right-icon-container"],
            "d-flex justify-content-center align-items-center flex-column overflow-hidden"
          )}
          onClick={btnClickEvent}
        >
          <span className="w-100 h-100">{arrowRight}</span>
          <span
            className="w-100 h-100 position-absolute"
            style={{ top: "-100%" }}
          >
            {arrowRight}
          </span>
        </div>
      </div>

      <div
        className={clsx(
          styles["footer-button-container"],
          "von-font text-center"
        )}
        ref={nextPageRef}
        style={{ opacity: 0, display: "none" }}
      >
        <h4>
          TURN <br /> THE PAGE
        </h4>
        <div
          className={clsx(
            styles["right-icon-container"],
            "d-flex justify-content-center align-items-center flex-column overflow-hidden ml-2"
          )}
        >
          <span className="w-100 h-100">{arrowRight}</span>
          <span
            className="w-100 h-100 position-absolute"
            style={{ top: "-100%" }}
          >
            {arrowRight}
          </span>
        </div>
      </div>
    </div>
  );
};

export default Home;
